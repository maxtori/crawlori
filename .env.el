(message "loading environment for crawlori")
(setenv "PGDATABASE" "crawlori")
(setenv "PPX_JSOO_FAKE" "false")
(setenv "PGCUSTOM_CONVERTERS_CONFIG" (format "%ssrc/pg/converters.sexp" (expand-file-name default-directory)))
(setenv "BMID" "0")
(setenv "TNAME" "dummy")
