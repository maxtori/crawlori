(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Crp
open Proto
module PG = EzPG_rp.Make(struct
    type err = error
    let from_exn = function
      | PGOCaml.Error s -> `pg_error s
      | PGOCaml.PostgreSQL_Error (e, l) -> `pg_psql (e, l)
      | exn -> `pg_exn exn
  end)

type init_acc = (int * (string list * string list)) list

let init ?hook ?n () =
  let n = match n with
    | Some n -> Some n
    | None -> match Sys.getenv_opt "PGLWTMAXCONNEXIONS" with
      | None -> None
      | Some s -> try Some (int_of_string s) with _ -> None in
  PG.Pool.init ?n ~database ();
  try
    let l = [0, (Tables.upgrade, Tables.downgrade)] in
    let>? l = match hook with
      | None -> rok l
      | Some f ->
        let|>? l = f l in
        Tables.merge l in
    let upgrades = List.map (fun (v, (u, downgrade)) ->
        v, (fun dbh version -> EzPG.upgrade ~dbh ~version ~downgrade u)) l in
    EzPGUpdater.main database ~upgrades;
    rok ()
  with exn -> rerr (`pg_exn exn)

module PGOCaml = PG.PGOCaml

type txn = PGOCaml.pa_pg_data PGOCaml.t

let handle : (txn -> 'a rp) -> 'a rp = PG.Pool.use

let transaction f =
  handle @@ fun dbh ->
  let>? () = PGOCaml.begin_work dbh in
  let> r = f dbh in match r with
  | Ok r ->
    let|>? () = PGOCaml.commit dbh in
    r
  | Error e ->
    let>? () = PGOCaml.rollback dbh in
    rerr e

let registered hash =
  handle @@ fun dbh ->
  let|>? r = [%pgsql dbh "select main from predecessors where hash = $hash"] in
  match r with
  | [ main ] -> true, main
  | _ -> false, false

let head dbh =
  let aux dbh =
    [%pgsql dbh "select hash, level from predecessors where main order by level desc limit 1"] in
  match dbh with
  | None ->
    let> r = handle aux in
    begin match r with Ok [ hash, level ] -> rok (hash, level) | _ -> rok ("", 0l) end
  | Some dbh ->
    let> r = aux dbh in
    match r with Ok [ hash, level ] -> rok (hash, level) | _ -> rerr `empty_database

let recursion dbh hash =
  [%pgsql dbh "select predecessor, main from predecessors where hash = $hash"]

let register_predecessor ~dbh b =
  let> r = [%pgsql dbh
      "insert into predecessors(hash, predecessor, level, main) \
       values(${b.hash}, ${b.header.shell.predecessor}, ${b.header.shell.level}, false) \
       on conflict do nothing returning hash"] in
  match r with
  | Ok [ _ ] -> rok `new_block
  | _ ->
    let>? r = [%pgsql dbh "select main from predecessors where hash = ${b.hash}"] in
    match r with
    | [ main ] -> rok (`already_known main)
    | _ -> rerr `wrong_db_state

let register ?block_hook ?operation_hook b =
  transaction @@ fun dbh ->
  let>? r = register_predecessor ~dbh b in
  match r with
  | `already_known main ->
    if main then rok `main else rok `alt
  | `new_block ->
    let block = Option.map (fun f -> (fun () b -> f dbh b)) block_hook in
    let operation = Option.map (fun f -> (fun () o -> f dbh o)) operation_hook in
    let|>? (), () = Utils.Process.block ?block ?operation ((), ()) b in
    `new_block

let set_main ?main ?hook dbh hash =
  let main = match main with Some _ -> true | None -> false in
  let>? () = [%pgsql dbh "update predecessors set main = $main where hash = $hash"] in
  match hook with
  | None -> rok (fun () -> rok ())
  | Some f -> f dbh {m_hash = hash; m_main = main}

let forward_end ?hook level =
  match hook with
  | None -> rok ()
  | Some f -> f level
