(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

let () =
  if Crp.no_update then ()
  else
    let upgrade =
      Tables.upgrade @ Pg_blocks_tables.upgrade @ Pg_operations_tables.upgrade @
      Pg_bigmap_tables.upgrade ~keep:false @ Pg_bigmaps_tables.upgrade ~keep:false in
    let downgrade =
      Tables.downgrade @ Pg_blocks_tables.downgrade @ Pg_operations_tables.downgrade @
      Pg_bigmap_tables.downgrade ~keep:false @ Pg_bigmaps_tables.downgrade in
    let upgrades = [0, (fun dbh version -> EzPG.upgrade ~dbh ~version ~downgrade upgrade)] in
    EzPGUpdater.main Crp.database ~upgrades
