let downgrade = [
  "drop index blocks_level_index";
  "drop index blocks_cycle_index";
  "drop table blocks"
]

let upgrade = [
  "create table blocks(\
   hash varchar primary key, \
   level int not null, \
   cycle int, \
   cycle_position int, \
   expected_commitment boolean, \
   predecessor varchar, \
   tsp timestamp not null, \
   main boolean not null, \
   protocol varchar not null, \
   chain_id varchar not null, \
   baker varchar, \
   validation_pass int not null, \
   operations_hash varchar not null, \
   fitness_version int not null, \
   round int not null, \
   pred_round int not null, \
   locked_round int, \
   context varchar not null, \
   proof_of_work_nonce varchar not null, \
   seed_nonce_hash varchar, \
   signature varchar not null)";
  "create index blocks_level_index on blocks(level)";
  "create index blocks_cycle_index on blocks(cycle)";
]
