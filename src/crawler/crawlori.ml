(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

include Crp
open Proto

let stop = ref false

module Request = Request
module Rp = Crp
module Plugins = Plugins

module Make(Db : S)(E : sig type extra end) = struct

  module Plugins = Plugins.Make(Db)(E)

  let reset_chain ?hook dbh after cross_hash =
    (* unvalidate blocks backward until switch *)
    let rec aux f hash =
      if hash <> cross_hash then (
        let>? r = Db.recursion dbh hash in
        let>? after = Db.set_main ?hook dbh hash in
        let after () =
          let>? () = f () in
          after () in
        match r with
        | [ pred, _ ] -> aux after pred
        | _ -> rok after)
      else rok f in
    let>? (hash, _) = Db.head (Some dbh) in
    aux after hash

  let update_chain ?hook hash pred_hash level =
    if !stop then rok ()
    else
      let>? after = Db.transaction (fun dbh ->
          let rec aux f hash pred_hash level =
            let>? r = Db.recursion dbh pred_hash in
            match r with
            | [ _, true ] ->
              let>? f = reset_chain ?hook dbh f pred_hash in
              let|>? after = Db.set_main ~main:(pred_hash, level) ?hook dbh hash in
              let f () = let>? () = f () in after () in
              pred_hash, level, f
            | [ pred, false ] ->
              let>? (ph, lv, f) = aux f pred_hash pred (Int32.pred level) in
              let|>? after = Db.set_main dbh ~main:(pred_hash, level) ?hook hash in
              let f () = let>? () = f () in after () in
              ph, lv, f
            | _ -> rerr (`wrong_crawler_state pred_hash) in
          Log.(p @@ s "update chain for %s (%ld)" (String.sub hash 0 10) level);
          let>? ph, lv, after = aux (fun () -> rok ()) hash pred_hash level in
          Log.(p ~color:`green @@ s "update chain ok  %s (%ld) -> %s (%ld)"
                 (String.sub ph 0 10) (Int32.pred lv)
                 (String.sub hash 0 10) level);
          rok after) in
      after ()

  let register_forward ?block_hook ?operation_hook ?main_hook ?main_not_forward_hook config ~start ~until =
    let rec aux counter level =
      if !stop then rok ()
      else
        let () = Log.(p ~before:"request " ~color:`cyan @@ s "level %ld" level) in
        let>? block = Request.block config (Int32.to_string level) in
        let>? known_in_main = Db.register ?block_hook ?operation_hook block in
        let>? () = match known_in_main with
          | `main -> rok ()
          | `new_block ->
            if level > start && (counter <= 1 || level = until) then
              update_chain ?hook:main_hook block.hash block.header.shell.predecessor block.header.shell.level
            else if level > start then rok ()
            else
              let>? after = Db.transaction (fun dbh ->
                  Db.set_main ~main:(block.header.shell.predecessor, block.header.shell.level)
                    ?hook:main_hook dbh block.hash) in
              after ()
          | `alt ->
            update_chain ?hook:main_not_forward_hook block.hash block.header.shell.predecessor block.header.shell.level in
        let counter = if counter <= 1 then config.step_forward else counter - 1 in
        if level < until then aux counter (Int32.succ level)
        else rok ()
    in
    if start > until then rok ()
    else aux config.step_forward start

  let counter = ref 1

  let rec register ?block_hook ?operation_hook ?main_hook config hash =
    if !stop then rok ()
    else
      let shash = String.sub hash 0 10 in
      let>? shell = Request.shell config hash in
      let predecessor_hash = shell.predecessor in
      let level = shell.level in
      let>? registered, _ = Db.registered predecessor_hash in
      let go_back = match config.start with
        | None -> not registered
        | Some start -> not registered && level > start in
      let>? () =
        if go_back then (
          Log.(p ~color:`yellow @@ s "%ld: missing predecessor %s" level predecessor_hash);
          register ?block_hook ?operation_hook ?main_hook config predecessor_hash)
        else rok () in
      let>? registered, known_in_main = Db.registered hash in
      let>? () =
        if not registered then (
          counter := 1;
          Log.(p ~before:"\nrequest " ~color:`cyan @@ s "%s (%ld)" shash level);
          let>? block = Request.block config hash in
          let>? _ = Db.register ?block_hook ?operation_hook block in
          rok ())
        else (
          incr counter;
          Log.(p ~en:false @@ s "%shead    %s (x%d)%!%s"
                 (if config.verbose > 0 then "" else if !counter = 2 then "\n" else "\r")
                 shash !counter (if config.verbose > 0 then "\n" else ""));
          rok ()) in
      if not known_in_main then
        update_chain ?hook:main_hook hash predecessor_hash level
      else rok ()

  let init config =
    let block_hook, operation_hook, main_hook, main_not_forward_hook, forward_hook, init_hook =
      Plugins.hooks ~forward:(not config.no_forward) config in
    let>? () = Db.init ?hook:init_hook () in
    let>? meta = Request.head_metadata config in
    let current_level = meta.Proto.meta_level.nl_level in
    Log.(p @@ s "Blockchain current level %ld@." current_level);
    let cl = Int32.(sub current_level (mul 2l config.confirmations_needed)) in
    let>? (_, ll) = Db.head None in
    Log.(p @@ s "Last registered level %ld@." ll);
    let until = match config.forward with
      | Some level -> min level cl
      | None -> max cl 1l in
    let>? start = match config.start with
      | Some level when level > until -> rerr (`start_level_too_recent (level, cl))
      | Some level -> rok @@ max (min level until) ll
      | None -> rok @@ min until ll in
    Log.(p @@ s "Registering forward from %ld to %ld@." start until);
    let>? () = register_forward ?block_hook ?operation_hook ?main_hook ?main_not_forward_hook
        config ~start ~until in
    Log.(p ~en:false @@ s "End of register forward@?");
    let>? () = Db.forward_end ?hook:forward_hook (min start until) in
    Log.(p @@ s " -> OK");
    rok ()

  let loop config =
    let block_hook, operation_hook, main_hook, _, _, _ = Plugins.hooks config in
    let rec aux () =
      if !stop then rok ()
      else
        let>? head_hash = Request.head_hash config in
        let>? () = register ?block_hook ?operation_hook ?main_hook config head_hash in
        let> () = EzLwtSys.sleep config.sleep in
        aux () in
    aux ()

  let reset_stop () = stop := false
  let stop () = stop := true

  let async_recrawl ~config ~start ?end_ ?block ?operation (acc_b, acc_o) =
    let aux_end_f () = match end_ with
      | None ->
        let|>? (_, aux_level) = Db.head None in
        aux_level
      | Some level -> rok level in
    let rec aux aux_end level (acc_b, acc_o) =
      if level > aux_end then
        let>? aux_end2 = aux_end_f () in
        if aux_end = aux_end2 then rok (acc_b, acc_o)
        else aux aux_end2 level (acc_b, acc_o)
      else
        let>? bl = Request.block config (Int32.to_string level) in
        let>? (acc_b, acc_o) = Utils.Process.block ?block ?operation (acc_b, acc_o) bl in
        aux aux_end (Int32.succ level) (acc_b, acc_o) in
    let>? aux_end = aux_end_f () in
    aux aux_end start (acc_b, acc_o)

end
