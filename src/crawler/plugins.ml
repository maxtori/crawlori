open Crp

module type S = sig
  type txn
  type extra
  type init_acc
  val name : string
  val always : bool
  val init : extra -> init_acc -> init_acc rp
  val register_operation : ?forward:bool -> extra -> txn -> block_op -> unit rp
  val register_block : ?forward:bool -> extra -> txn -> Proto.full_block -> unit rp
  val set_main : ?forward:bool -> extra ->  txn -> main_arg -> (unit -> unit rp) rp
  val forward_end : extra -> int32 -> unit rp
end

module Make(Db : sig type txn type init_acc end)(E : sig type extra end)  = struct
  module type S = S

  type plugin = {
    name : string;
    always : bool;
    init: E.extra -> Db.init_acc -> Db.init_acc rp;
    register_operation : ?forward:bool -> E.extra -> Db.txn -> block_op -> unit rp;
    register_block : ?forward:bool -> E.extra -> Db.txn -> Proto.full_block -> unit rp;
    set_main : ?forward:bool -> E.extra ->  Db.txn -> main_arg -> (unit -> unit rp) rp;
    forward_end : E.extra -> int32 -> unit rp;
  }

  let plugins : plugin list ref = ref []

  let register_record r = plugins := !plugins @ [ r ]

  let register ?(always=false) ?operation ?block ?main ?forward ?init name =
    let init = Option.value ~default:(fun _ a -> rok a) init in
    let register_operation =
      Option.value ~default:(fun ?forward:_ _config _dbh _op -> rok ()) operation in
    let register_block =
      Option.value ~default:(fun ?forward:_ _config _dbh _b -> rok ()) block in
    let set_main =
      Option.value ~default:(fun ?forward:_ _config _dbh _m -> rok (fun () -> rok ())) main in
    let forward_end = Option.value ~default:(fun _config _lv -> rok ()) forward in
    register_record {
      name; always; init; register_operation; register_block; set_main; forward_end;
    }

  let register_mod (module S : S with type txn = Db.txn and type extra = E.extra and type init_acc = Db.init_acc) =
    register_record {
      name = S.name; always = S.always; init = S.init;
      register_operation = S.register_operation; register_block = S.register_block;
      set_main = S.set_main; forward_end = S.forward_end
    }

  let loaded l =
    List.filter (fun p -> p.always || List.mem p.name l) !plugins

  let operation_hook ?forward e = function
    | [] -> None
    | l -> Some (fun dbh op ->
        fold (fun () p -> p.register_operation ?forward e dbh op) () l)

  let block_hook ?forward e = function
    | [] -> None
    | l -> Some (fun dbh op ->
        fold (fun () p -> p.register_block ?forward e dbh op) () l)

  let main_hook ?forward e = function
    | [] -> None
    | l -> Some (fun dbh op ->
        fold (fun g p ->
            let>? h = p.set_main ?forward e dbh op in
            let g () = let>? () = g () in h () in
            rok g) (fun () -> rok ()) l)

  let forward_hook e = function
    | [] -> None
    | l -> Some (fun level -> fold (fun () p -> p.forward_end e level) () l)

  let init_hook e = function
    | [] -> None
    | l -> Some (fun acc -> fold (fun acc p -> p.init e acc) acc l)

  let hooks ?forward config =
    let loaded = loaded config.plugins in
    let block = block_hook ?forward config.extra loaded in
    let operation = operation_hook ?forward config.extra loaded in
    let main = main_hook ?forward config.extra loaded in
    let main_not_forward = main_hook ~forward:false config.extra loaded in
    let forward_end = forward_hook config.extra loaded in
    let init = init_hook config.extra !plugins in
    block, operation, main, main_not_forward, forward_end, init

end
