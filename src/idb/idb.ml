(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Crp
open Proto
open Proto_jsoo
open Ezjs_min
open Ezjs_idb

type init_acc = unit

type predecessor = {
  pr_hash: Proto.A.block_hash;
  pr_pred: Proto.A.block_hash;
  pr_level: int32;
  pr_main: bool;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type operation = {
  op_key: string;
  op_level: int32;
  op_tsp: Proto.A.timestamp;
  op_op: Proto.micheline Proto.manager_operation list;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type head = {
  h_hash: Proto.A.block_hash;
  h_level: int32;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type level = {
  lv_key : int32;
  lv_main : Proto.A.block_hash option;
  lv_uncles : Proto.A.block_hash list;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

module PredTr : Tr_sig with type js = predecessor_jsoo t and type t = predecessor = struct
  type js = predecessor_jsoo t
  type t = predecessor
  let to_js = predecessor_to_jsoo
  let of_js = predecessor_of_jsoo
end

module BlockTr : Tr_sig with type js = block_jsoo t and type t = block = struct
  type js = block_jsoo t
  type t = block
  let to_js = block_to_jsoo
  let of_js = block_of_jsoo
end

module OpTr : Tr_sig with type js = operation_jsoo t and type t = operation = struct
  type js = operation_jsoo t
  type t = operation
  let to_js = operation_to_jsoo
  let of_js = operation_of_jsoo
end

module HeadTr : Tr_sig with type js = head_jsoo t and type t = head = struct
  type js = head_jsoo t
  type t = head
  let to_js = head_to_jsoo
  let of_js = head_of_jsoo
end

module LvTr : Tr_sig with type js = level_jsoo t and type t = level = struct
  type js = level_jsoo t
  type t = level
  let to_js = level_to_jsoo
  let of_js = level_of_jsoo
end

module PredStore = Store(StringTr)(PredTr)
module BlockStore = Store(StringTr)(BlockTr)
module OpStore = Store(StringTr)(OpTr)
module HeadStore = Store(NoTr(struct type t = unit end))(HeadTr)
module LvStore = Store(IntTr)(LvTr)

let db : Types.iDBDatabase t ref = ref (Unsafe.obj [||])

let init ?hook:_ ?n:_ () =
  PredStore.set_name "predecessors";
  BlockStore.set_name "blocks";
  OpStore.set_name "operations";
  LvStore.set_name "levels";
  HeadStore.set_name "head";
  let upgrade db e =
    if e.new_version >= 1 && e.old_version = 0 then
      let options = {key_path=Some "hash"; auto_increment= None} in
      ignore @@ BlockStore.create ~options db;
      ignore @@ PredStore.create ~options db;
      ignore @@ HeadStore.create db;
      let options = {key_path=Some "key"; auto_increment= None} in
      ignore @@ OpStore.create db ~options;
      ignore @@ LvStore.create db ~options in
  rok @@ openDB ~upgrade ~version:1 database (fun d -> db := d)

type txn = Types.iDBTransaction t

let store (type k) (type d) ?txn
    (module St : S with type K.js = k and type D.js = d) : (k, d) Types.iDBObjectStore t =
  St.store ?tx:txn ~mode:READWRITE !db

let transaction f =
  f (create_transaction ~mode:READWRITE !db [
      "predecessors"; "blocks"; "operations"; "levels"; "head" ])

let to_rp_error e =
  match AOpt.to_option e with
  | None -> `idb_error None
  | Some e ->
    `idb_error (Some (AOpt.to_option e##.code,
                      to_string e##.name, to_string e##.message))

let error n r = Lwt.wakeup n (Error (to_rp_error r))

let get (type k) (type d) ?txn
    (module St : S with type K.t = k and type D.t = d)
    (k : k) : d rp =
  let st = store ?txn (module St) in
  let w, n = Lwt.wait () in
  St.get st ~error:(error n) (function
      | None -> Lwt.wakeup n (Error (`idb_error None))
      | Some v -> Lwt.wakeup n (Ok v)) (St.K k);
  w

let add (type k) (type d) ?txn ?key
    (module St : S with type K.t = k and type D.t = d)
    (v : d) : unit rp =
  let st = store ?txn (module St) in
  let w, n = Lwt.wait () in
  St.add st ?key ~callback:(fun _b -> Lwt.wakeup n (Ok ()))  ~error:(error n) v;
  w

let set (type k) (type d) ?txn ?key
    (module St : S with type K.t = k and type D.t = d)
    (v : d) : unit rp =
  let st = store ?txn (module St) in
  let w, n = Lwt.wait () in
  St.put st ?key ~callback:(fun _ -> Lwt.wakeup n (Ok ()))  ~error:(error n) v;
  w

let remove (type k) (type d) ?txn
    (module St : S with type K.t = k and type D.t = d)
    (k : k) : unit rp =
  let st = store ?txn (module St) in
  let w, n = Lwt.wait () in
  St.delete st ~error:(error n) ~callback:(fun _ -> Lwt.wakeup n (Ok ()))
    (St.K k);
  w

(** main *)

let register_predecessor ~txn b =
  let> r = get ~txn (module PredStore) b.hash in match r with
  | Ok {pr_main; _} -> rok (`already_known pr_main)
  | Error _ ->
    let>? () = add ~txn (module PredStore) {
        pr_hash = b.hash; pr_pred = b.header.shell.predecessor;
        pr_level = b.header.shell.level; pr_main = false } in
    rok `new_block

let register ?block_hook ?operation_hook b =
  transaction @@ fun txn ->
  let>? r = register_predecessor ~txn b in
  match r with
  | `already_known main -> if main then rok `main else rok `alt
  | `new_block ->
    let block = Option.map (fun f -> (fun () b -> f txn b)) block_hook in
    let operation = Option.map (fun f -> (fun () o -> f txn o)) operation_hook in
    let|>? (), () = Utils.Process.block ?block ?operation ((), ()) b in
    `new_block

let registered hash =
  let> r = get (module PredStore) hash in match r with
  | Error _ -> rok (false, false)
  | Ok {pr_main; _ } -> rok (true, pr_main)

let head txn =
  let> r = get (module HeadStore) ?txn () in match r with
  | Ok {h_hash; h_level} -> rok (h_hash, h_level)
  | Error _ -> match txn with
    | None -> rok ("", 0l)
    | Some _ -> rerr `empty_database

let recursion txn hash =
  let> r = get (module PredStore) ~txn hash in match r with
  | Ok {pr_pred; pr_main; _} -> rok [ pr_pred, pr_main ]
  | Error _ -> rok []

let set_main ?main ?hook txn hash =
  let main = match main with Some _ -> true | None -> false in
  let>? {pr_pred; pr_level; _} = get (module PredStore) ~txn hash in
  let> _ = set (module PredStore) {pr_hash=hash; pr_pred; pr_level; pr_main=main} in
  match hook with
  | None -> rok (fun () -> rok ())
  | Some f -> f txn {m_hash = hash; m_main = main}

let forward_end ?hook level =
  match hook with
  | None -> rok ()
  | Some f -> f level
