(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Tzfunc

type nonrec exn = Rp.exn [@encoding Rp.exn_enc.Proto.json] [@@deriving encoding, jsoo {modules=[Rp, Rp_jsoo]}]

type crawlori_error = [
  | `pg_exn of exn [@kind]
  | `pg_error of (string [@obj1 "msg"]) [@kind]
  | `pg_psql of (((string [@key "error"]) * ((char * string) list [@key "fields"])) [@object]) [@kind]
  | `empty_database [@kind]
  | `request_error of ((int [@key "code"]) * (string option [@key "msg"] [@opt]) [@object]) [@kind]
  | `request_exn of exn [@kind]
  | `wrong_db_state [@kind]
  | `wrong_crawler_state of (string [@obj1 "block_hash"]) [@kind]
  | `no_node_address [@kind]
  | `cannot_parse_config of exn [@kind]
  | `lmdb_exn of exn [@kind]
  | `txn_aborted [@kind]
  | `binary_read_error of (string [@obj1 "msg"]) [@kind]
  | `binary_read_exn of exn [@kind]
  | `binary_write_error of (string [@obj1 "msg"]) [@kind]
  | `binary_write_exn of exn [@kind]
  | `binary_recursive_error [@kind]
  | `no_config [@kind]
  | `gadt_error [@kind]
  | `idb_error of ((int option * string * string) option [@obj1 "error"]) [@kind]
  | `library_not_installed of (string [@obj1 "library"]) [@kind]
  | `js_error of (string [@obj1 "error"]) [@kind]
  | `start_level_too_recent of ((int32 [@key "start"]) * (int32 [@key "allowed"]) [@object]) [@kind]
  | `block_without_metadata [@kind]
  | `caqti_error of (string [@wrap "error"]) [@kind]
  | `caqti_exn of (exn [@wrap "exn"]) [@kind]
  | Rp.error [@encoding Tzfunc.Rp.error_enc.Proto.Encoding.json] [@wrap "tzfunc"]
] [@@deriving encoding {title="crawlori_error"}, jsoo {modules=[Rp, Rp_jsoo]}]

type error = crawlori_error [@@deriving encoding, jsoo]

type 'a rp = ('a, error) result Lwt.t

let string_of_error e =
  EzEncoding.construct ~compact:false error_enc e

let rok = Lwt.return_ok
let rerr = Lwt.return_error
let of_lwt p = Lwt.bind p rok

let print_error err =
  Log.(e ~color:`red ~after:(s "\n%s" (string_of_error err)) @@ s "Error:")

let print_rp ?print_ok p =
  Lwt.bind p @@ function
  | Ok x ->
    (match print_ok with None -> () | Some f -> f x);
    rok x
  | Error e ->
    print_error e;
    rerr e

let (let>) = Tzfunc.Rp.(let>)
let (let|>) = Tzfunc.Rp.(let|>)
let (let>?) = Tzfunc.Rp.(let>?)
let (let|>?) = Tzfunc.Rp.(let|>?)
let (let$) = Tzfunc.Rp.(let$)
let (let|$) = Tzfunc.Rp.(let|$)

let rec fold f acc = function
  | [] -> rok acc
  | h :: t ->
    Lwt.bind (f acc h) @@ function
    | Error e -> rerr e
    | Ok acc -> fold f acc t

let iter f l = fold (fun () x -> f x) () l

let map f l =
  let rec aux acc = function
    | [] -> rok acc
    | h :: t ->
      Lwt.bind (f h) (function
          | Error e -> rerr e
          | Ok x -> aux (x :: acc) t) in
  Lwt.map (Result.map List.rev) (aux [] l)

type block_op = {
  bo_block : Proto.A.block_hash;
  bo_level : int32;
  bo_tsp : Proto.A.timestamp;
  bo_hash : Proto.A.operation_hash;
  bo_op : Proto.micheline Proto.manager_operation_info;
  bo_counter : Z.t;
  bo_meta : Proto.op_metadata option;
  bo_numbers : Proto.manager_operation_numbers option;
  bo_nonce : int32 option;
  bo_index : int32;
  bo_indexes : int32 * int32 * int32;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type main_arg = {
  m_main : bool;
  m_hash : Proto.A.block_hash;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type db_kind = [`pg | `lmdb | `idb | `caqti] [@enum]
[@@deriving encoding, jsoo]

module SSet = struct
  include Set.Make(String)
  let enc = Json_encoding.(conv elements of_list (list string))
end

type 'extra config = {
  nodes: string list; [@dft []]
  sleep: float; [@dft 5.]
  start: int32 option; [@opt]
  forward: int32 option; [@opt]
  verbose: int; [@dft 0]
  db_kind: db_kind; [@dft `pg]
  step_forward: int; [@dft 100]
  confirmations_needed: int32; [@dft 2l]
  plugins: string list; [@dft []]
  retry: int option;
  retry_timeout: float; [@dft 5.]
  no_forward: bool; [@dft false]
  extra: 'extra [@merge]
} [@@deriving encoding, jsoo]

module type S = sig
  type txn
  type init_acc
  val init : ?hook:(init_acc -> init_acc rp) -> ?n:int -> unit -> unit rp
  val register :
    ?block_hook:(txn -> Proto.full_block -> unit rp) ->
    ?operation_hook:(txn -> block_op -> unit rp) ->
    Proto.full_block -> [`new_block | `main | `alt ] rp
  val registered : Proto.A.block_hash -> (bool * bool) rp
  val head : txn option -> (string * int32) rp
  val transaction : (txn -> 'a rp) -> 'a rp
  val set_main : ?main:(Proto.A.block_hash * int32) ->
    ?hook:(txn -> main_arg -> (unit -> unit rp) rp) ->
    txn -> Proto.A.block_hash -> (unit -> unit rp) rp
  val recursion : txn -> string -> (Proto.A.block_hash * bool) list rp
  val forward_end : ?hook:(int32 -> unit rp) -> int32 -> unit rp
end

module Dummy = struct
  let lib = ref ""

  type txn = unit
  type init_acc = unit
  let init ?hook:_ ?n:_ () = rerr @@ `library_not_installed !lib
  let register ?block_hook:_ ?operation_hook:_ _ =  rerr @@ `library_not_installed !lib
  let registered _ = rerr @@ `library_not_installed !lib
  let head _ = rerr @@ `library_not_installed !lib
  let transaction _ = rerr @@ `library_not_installed !lib
  let set_main ?main:_ ?hook:_ _ _ = rerr @@ `library_not_installed !lib
  let recursion _ _ = rerr @@ `library_not_installed !lib
  let forward_end ?hook:_ _ = rerr @@ `library_not_installed !lib
end

module Dummy_plugin = struct
  let lib = ref ""

  type txn = unit
  let always = false
  let name = "dummy"
  let register_operation ?forward:_ _ _ _ = rerr @@ `library_not_installed !lib
  let forward_end _config _level = rerr @@ `library_not_installed !lib
  let init _ = rerr @@ `library_not_installed !lib
  let register_block ?forward:_ _ _ _ = rerr @@ `library_not_installed !lib
  let set_main ?forward:_ _ _ _ = rerr @@ `library_not_installed !lib
end


let database = Option.value ~default:Db_env.database (Sys.getenv_opt "PGDATABASE")
let no_update = Db_env.no_update
