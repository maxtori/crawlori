DB ?= crawlori
CRAWLORI_ROOT ?= .
IMAGE ?= crawlori:latest
POSTGRES_USER ?= postgres

-include Makefile.config

all: copy

build:
	@PGDATABASE=$(DB) PGCUSTOM_CONVERTERS_CONFIG=$(CRAWLORI_ROOT)/src/pg/converters.sexp dune build --profile release src

copy: build
	@mkdir -p _bin
	@cp -f _build/default/src/crawler/unix/main.exe _bin/crawlori
	@mkdir -p _dist
	@cp -f _build/default/src/crawler/js/main.bc.js _dist/crawlori.js

clean:
	@dune clean

install:
	@PGDATABASE=crawlori_tmp PGCUSTOM_CONVERTERS_CONFIG=$(CRAWLORI_ROOT)/src/pg/converters.sexp dune build -p crawlori @install
	@-dropdb crawlori_tmp

drop-pg:
	@dropdb $(DB)
	@rm -f _build/default/src/pg/.db_witness

drop-lmdb:
	@rm -f .lmdb_$(DB)*

deps:
	@opam install --deps-only .

pg-user:
	@sudo -i -u $(POSTGRES_USER) -- psql -c 'create user ${USER} with createdb;'

pg-deps: deps
	@opam install ocurl pgocaml pgocaml_ppx ez_pgocaml

lmdb-deps: deps
	@opam install ocurl lmdb

idb-deps: deps
	@opam install ezjs_idb js_of_ocaml-lwt

caqti-deps: deps
	@opam install ocurl caqti-lwt

excraw-pg:
	@CRAWLORI_NO_UPDATE=true PGDATABASE=excraw PGCUSTOM_CONVERTERS_CONFIG=$(CRAWLORI_ROOT)/src/pg/converters.sexp dune build example/pg/excraw_pg_update.exe
	@CRAWLORI_NO_UPDATE=true PGDATABASE=excraw PGCUSTOM_CONVERTERS_CONFIG=$(CRAWLORI_ROOT)/src/pg/converters.sexp dune build example/pg

excraw-caqti:
	@dune build example/caqti

_opam:
	opam switch create . 4.13.1

build-docker:
	docker build -t $(IMAGE) -f docker/Dockerfile .
